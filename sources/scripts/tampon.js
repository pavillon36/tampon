		
		$(function() {
			var handlelines = $( "#handle-lines" );
			var handlebordersize = $( "#handle-bordersize" );
			var handlefontsize = $( "#handle-fontsize" );
			var handlewidth = $( "#handle-width" );
			var handleheight = $( "#handle-height" );
			var handlerotate = $( "#handle-rotate" );
			
			// Overrides Maths.sin function, because it uses radians
			function sin(angleDegrees) {
			    return Math.sin(angleDegrees*Math.PI/180);
			};

			// Overrides Maths.cos function, because it uses radians
			function cos(angleDegrees) {
			    return Math.cos(angleDegrees*Math.PI/180);
			};
			
			// Translate rgb to hex
			function hexFromRGB(r, g, b) {
				var hex = [
					r.toString( 16 ),
					g.toString( 16 ),
					b.toString( 16 )
				];
				$.each( hex, function( nr, val ) {
					if ( val.length === 1 ) {
						hex[ nr ] = "0" + val;
					}
				});
				return hex.join( "" ).toUpperCase();
			};

			// Set the sizes of the svg components
			function setSizes(rotateAngle, newWidth, newHeight){
				// Change the sign of the rotateAngle, to have a more natural rotation
				rotateAngle = (-rotateAngle);
				// The svg tag svgout is the container of the stamp. It has to be wide enough to contain the stamp, even when it is rotated.
				// The width is calculated 
				var svgoutWidth  = Math.abs((newWidth+20) * cos(rotateAngle)) + Math.abs((newHeight+20) * sin(rotateAngle));
				var svgoutHeight = Math.abs((newWidth+20) * sin(rotateAngle)) + Math.abs((newHeight+20) * cos(rotateAngle));
				$( "#svgout"  ).attr("width",  svgoutWidth);
				$( "#svgout"  ).attr("height",  svgoutHeight);
				// Translation before rotation to definie the new 0 0 of the group
				// The group has to be centered in the svgout
				var translateX = Math.floor((svgoutWidth - newWidth) / 2);
				var translateY = Math.floor((svgoutHeight - newHeight) / 2);
				// The center of the rotation is the center of the group : width/2 height/2
				$( "#group"  ).attr("transform", "translate("+ translateX +","+ translateY +") rotate("+ rotateAngle +" "+ Math.floor(newWidth/2) +" "+ Math.floor(newHeight/2) +")");
				$( "#rectin"  ).attr("width", newWidth );
				$( "#rectin"  ).attr("height", newHeight );
				$( "#svgin"   ).attr("width", newWidth );
				$( "#svgin"   ).attr("height", newHeight );
			};

			// Get the values from the color sliders and set the colors on the svg
			function changeColor() {
				var red = $( "#red" ).slider( "value" ),
					green = $( "#green" ).slider( "value" ),
					blue = $( "#blue" ).slider( "value" ),
					hex = hexFromRGB( red, green, blue );
				$( "#rectin"  ).attr("stroke", "#" + hex );
				$( "#svgline1" ).attr("fill",  "#" + hex );
				$( "#svgline1" ).attr("stroke",  "#" + hex );
				$( "#svgline2" ).attr("fill",  "#" + hex );
				$( "#svgline2" ).attr("stroke",  "#" + hex );
				$( "#svgline3" ).attr("fill",  "#" + hex );
				$( "#svgline3" ).attr("stroke",  "#" + hex );
			}
			
			$( "#lines" ).slider({
				value: 1,
				min: 1,
				max: 3,
				create: function() {
					handlelines.text( $( this ).slider( "value" ) );
				},
				slide: function( event, ui ) {
					handlelines.text( ui.value );
					switch(Number(ui.value)) {
  						case 1:
						    $( "#labelText1" ).text("Text:");
						    $( "#trText2" ).attr("style", "display:none;");
						    $( "#trText3" ).attr("style", "display:none;");

						    $( "#svgline1" ).attr("y", "50%");
						    $( "#svgline2" ).attr("visibility", "hidden");
						    $( "#svgline3" ).attr("visibility", "hidden");

						    break;
  						case 2:
						    $( "#labelText1" ).text("Line 1:");
						    $( "#trText2" ).attr("style", "");
						    $( "#trText3" ).attr("style", "display:none;");
						    $( "#svgline1" ).attr("y", "30%");
						    $( "#svgline2" ).attr("visibility", "visible");
						    $( "#svgline2" ).attr("y", "70%");
						    $( "#svgline3" ).attr("visibility", "hidden");
						    break;
  						case 3:
						    $( "#labelText1" ).text("Line 1:");
						    $( "#trText2" ).attr("style", "");
						    $( "#trText3" ).attr("style", "");
						    $( "#svgline1" ).attr("y", "25%");
						    $( "#svgline2" ).attr("visibility", "visible");
						    $( "#svgline2" ).attr("y", "50%");
						    $( "#svgline3" ).attr("visibility", "visible");
						    $( "#svgline3" ).attr("y", "75%");
						    break;
						} 
				}
			});
			
			$( "#red, #green, #blue" ).slider({
				orientation: "horizontal",
				range: "min",
				max: 255,
				value: 127,
				slide: changeColor,
				change: changeColor
			});

			$( "#red" ).slider( "value", 255 );

			$( "#green" ).slider( "value", 0 );

			$( "#blue" ).slider( "value", 0 );

			$( "#bordersize" ).slider({
				value: 10,
				min: 0,
				max: 20,
				create: function() {
					handlebordersize.text( $( this ).slider( "value" ) );
				},
				slide: function( event, ui ) {
					handlebordersize.text( ui.value );
					$( "#rectin" ).attr("stroke-width",  ui.value );
				}
			});

			$( "#fontsize" ).slider({
				value: 60,
				min: 0,
				max: 100,
				create: function() {
					handlefontsize.text( $( this ).slider( "value" ) );
				},
				slide: function( event, ui ) {
					handlefontsize.text( ui.value );
					$( "#svgline1" ).attr("font-size",  ui.value );
					$( "#svgline2" ).attr("font-size",  ui.value );
					$( "#svgline3" ).attr("font-size",  ui.value );
				}
			});

			$( "#width" ).slider({
				value: 200,
				min: 0,
				max: 500,
				create: function() {
					handlewidth.text( $( this ).slider( "value" ) );
				},
				slide: function( event, ui ) {
					handlewidth.text( ui.value );
					setSizes(Number($( "#rotate" ).slider("value")), Number(ui.value), Number($( "#height" ).slider("value")));
				}
			});

			$( "#height" ).slider({
				value: 100,
				min: 0,
				max: 500,
				create: function() {
					handleheight.text( $( this ).slider( "value" ) );
				},
				slide: function( event, ui ) {
					handleheight.text( ui.value );
					setSizes(Number($( "#rotate" ).slider("value")), Number($( "#width" ).slider("value")), Number(ui.value));
				}
			});

			$( "#rotate" ).slider({
				value: 0,
				min: -180,
				max: 180,
				create: function() {
					handlerotate.text( $( this ).slider( "value" ) );
				},
				slide: function( event, ui ) {
					handlerotate.text( ui.value );
					setSizes(Number(ui.value), Number($( "#width" ).slider("value")), Number($( "#height" ).slider("value")));
				}
			});

			$( "#text1" ).keyup(function() {
				var value = $( this ).val();
				$( "#svgline1" ).text( value );
			}).keyup();
	
			$( "#text2" ).keyup(function() {
				var value = $( this ).val();
				$( "#svgline2" ).text( value );
			}).keyup();

			$( "#text3" ).keyup(function() {
				var value = $( this ).val();
				$( "#svgline3" ).text( value );
			}).keyup();

			$("#btn-Convert-PNG").on('click', function () {
					html2canvas(document.querySelector( "#divsvgout" ),{ allowTaint: true, backgroundColor: "rgba(0,0,0,0)", removeContainer: true}).then(function(canvas) {
					var link = document.createElement("a");
					document.body.appendChild(link);
					link.download = "tampon.png";
					link.href = canvas.toDataURL("image/png");
					link.target = '_blank';
					link.click();
				});
			});

			$("#btn-Convert-JPG").on('click', function () {
					html2canvas(document.querySelector( "#divsvgout" )).then(function(canvas) {
					var link = document.createElement("a");
					document.body.appendChild(link);
					link.download = "tampon.jpg";
					link.href = canvas.toDataURL("image/jpeg");
					link.target = '_blank';
					link.click();
				});
			});
		});

