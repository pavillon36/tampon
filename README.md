Tampon
======

#### Fake rubber stamp creator ####
Deployed on https://www.pavillon36.net/tampon/

### Dependancies ###
This project uses :
* JQuery : (https://jquery.com)
* JQueryUI : (https://jqueryui.com)
* Html2Canvas : (https://html2canvas.hertzen.com)
